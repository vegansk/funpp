#ifndef LOCK_STRATEGY_HPP
#define LOCK_STRATEGY_HPP

#include <mutex>

namespace funpp {

struct no_lock {
    no_lock() {}
    no_lock(const no_lock&) = delete;
    ~no_lock() {}

    void lock() {}
    void unlock() {}
};

struct mutex_lock {
private:
    std::mutex _m;

public:
    mutex_lock() {}
    mutex_lock(const mutex_lock&) = delete;
    ~mutex_lock() {}

    void lock() {
        _m.lock();
    }

    void unlock() {
        _m.unlock();
    }
};

template<typename lock_strategy> class use_lock {
private:
    lock_strategy &_s;

public:
    use_lock(lock_strategy &s) : _s(s) {
        _s.lock();
    }

    ~use_lock() {
        _s.unlock();
    }
};

}

#endif // LOCK_STRATEGY_HPP
