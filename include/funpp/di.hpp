#ifndef __FUNPP_DI_HPP
#define __FUNPP_DI_HPP

#include <memory>
#include <map>
#include <string>
#include <typeinfo>
#include <typeindex>
#include <functional>
#include "lock_strategy.hpp"

namespace funpp {

struct prototype {};
struct session {};
struct singleton {};

struct std_init {};
struct custom_init {};

class service_base {
public:
    virtual ~service_base() {}
};

/**
 * Service locator based on templates
 */
template<typename service_cfg,
         typename service_base_class = service_base,
         template<typename X> class ptr = std::shared_ptr,
         typename lock_strategy = mutex_lock>
class service_locator : private service_cfg {
public:
    service_locator() {}
    service_locator(const service_locator&) = delete;

    typedef std::string session_id;

    template<class X> using ref_type = ptr<X>;

    template<class X> using service = typename service_cfg::template service<X>::type;
    template<class X> using scope = typename service_cfg::template service<X>::scope;
    template<class X> using init_type = typename service_cfg::template service<X>::init_type;

    template<typename svc> ptr<svc> get() {
        return _get<svc>(scope<svc>());
    }

    template<typename svc> ptr<svc> get(session_id sid) {
        return _get<svc>(sid, scope<svc>());
    }

    void drop_session(session_id sid) {
        _ss.erase(sid);
    }

private:
    typedef std::map<std::type_index, ptr<service_base_class>> services;
    typedef std::map<session_id, services> session_services;

    template<typename svc> svc* _create(std_init) {
        return new service<svc>;
    }

    template<typename svc> svc* _create(custom_init) {
        return service_cfg::template service<svc>::create();
    }

    template<typename svc> ptr<svc> _get(prototype) {
        return ptr<svc>(_create<svc>(init_type<svc>()));
    }

    lock_strategy _ssLock;
    session_services _ss;

    template<typename svc> ptr<svc> _get(session_id sid, session) {
        auto sIt = _ss.find(sid);
        if(_ss.end() == sIt) {
            use_lock<lock_strategy>{_ssLock};
            _ss[sid] = services();
        }

        auto &s = _ss[sid];

        auto it = s.find(std::type_index{typeid(svc)});
        if(s.end() == it) {
            use_lock<lock_strategy>{_ssLock};
            it = s.find(std::type_index{typeid(svc)});
            if(s.end() == it) {
                auto obj = _get<svc>(prototype());
                s.insert(std::make_pair(std::type_index{typeid(svc)}, std::dynamic_pointer_cast<service_base_class>(obj)));
                return obj;
            }
        }

        return std::dynamic_pointer_cast<svc>((*it).second);
    }

    lock_strategy _sLock;
    services _s;

    template<typename svc> ptr<svc> _get(singleton) {
        auto it = _s.find(std::type_index{typeid(svc)});
        if(_s.end() == it) {
            use_lock<lock_strategy>{_sLock};
            it = _s.find(std::type_index{typeid(svc)});
            if(_s.end() == it) {
                auto obj = _get<svc>(prototype());
                _s.insert(std::make_pair(std::type_index{typeid(svc)}, std::dynamic_pointer_cast<service_base_class>(obj)));
                return obj;
            }
        }

        return std::dynamic_pointer_cast<svc>((*it).second);
    }
};

class service_cfg_base {
public:
    template<typename svc> struct service {
        typedef void* type;
        typedef void scope;
        typedef void init_type;
    };
};

#define configure_service(cfg, svc, svcImpl, svcScope) template<> struct cfg::service<svc> { \
    typedef svcImpl type; \
    typedef svcScope scope; \
    typedef std_init init_type; \
    }

#define configure_service_with_params(cfg, svc, svcImpl, svcScope, ...) template<> struct cfg::service<svc> { \
    typedef svcImpl type; \
    typedef svcScope scope; \
    typedef custom_init init_type; \
    static svc* create() { \
        return new svcImpl{__VA_ARGS__}; \
    } \
    }

#define configure_service_with_func(cfg, svc, svcImpl, svcScope, func) template<> struct cfg::service<svc> { \
    typedef svcImpl type; \
    typedef svcScope scope; \
    typedef custom_init init_type; \
    static svc* create() { \
        return static_cast<svc*>((func())); \
    } \
    }

template<typename cfg> extern service_locator<cfg>& getServiceLocator();

template<typename svc, typename cfg> struct service_holder {
    typedef typename service_locator<cfg>::template ref_type<svc> svc_ref;

    svc_ref _svc;

    void try_create(service_locator<cfg>& c) {
        if(!_svc)
            _svc = c.template get<svc>();
    }

    svc* get(service_locator<cfg>& c) {
        if(!_svc)
            try_create(c);
        return _svc.get();
    }
};

template<typename svc, typename cfg, typename holder> class service_ref_base {
private:
    holder _hld;
    service_locator<cfg> &_cfg;

public:
    service_ref_base(service_locator<cfg> &c = getServiceLocator<cfg>()) : _cfg(c) {}
    ~service_ref_base() {}

    svc* get() {
        return _hld.get(_cfg);
    }

    svc* operator ->() {
        return get();
    }

    svc& operator *() {
        return *get();
    }
};

template<typename svc, typename cfg> using service_ref = service_ref_base<svc, cfg, service_holder<svc, cfg>>;

}

#endif // __FUNPP_DI_HPP
