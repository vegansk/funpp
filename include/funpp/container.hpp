#ifndef __FUNPP_CONTAINER_HPP
#define __FUNPP_CONTAINER_HPP

#include <algorithm>
#include <functional>
#include <initializer_list>

namespace funpp {

template<typename T, template <typename T, class = std::allocator<T>> class C> class container {
private:
    C<T> _c;
    template<typename,template<typename,class> class> friend class container;

public:
    container(): _c() {}
    container(const C<T> &l) : _c(l) {}
    container(container<T,C> &&l) : _c(move(l._c)) {}
    container(std::initializer_list<T> l) : _c(l) {}

    ~container() {}

    C<T>& get_c() {
        return _c;
    }

    template<typename N, typename F> container<N,C> map(F func) {
        container<N,C> res;
        std::for_each(_c.begin(), _c.end(), [&](T v) {
            res._c.push_back(func(v));
        });
        return res;
    }

    template<typename N> container<N,C> map(std::function<N(T)> func) {
        container<N,C> res;
        std::for_each(_c.begin(), _c.end(), [&](T v) {
            res._c.push_back(func(v));
        });
        return res;
    }

    template<typename F> container<T,C>& for_each(F func) {
        std::for_each(_c.begin(), _c.end(), func);
        return *this;
    }

};

}

#endif//__FUNPP_CONTAINER_HPP
