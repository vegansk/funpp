#ifndef DITEST_H
#define DITEST_H

#include <QObject>

class DiTest : public QObject {
    Q_OBJECT
public:
    explicit DiTest(QObject *parent = 0);

signals:

private slots:
    void serviceLocator();
    void serviceRef();
    void sessionService();
    void sessionServiceRef();
};

#endif // DITEST_H
