#include "ditest.h"
#include <QtTest/QtTest>

#include <funpp/di.hpp>
#include <string>

class PrintService : public funpp::service_base {
public:
    virtual void printMessage(const std::string &msg) = 0;
};

class SessionService : public funpp::service_base {
public:
    virtual void ping() = 0;
    virtual void checkPingCount(int pings) = 0;
};

class PrintServiceTestImpl : public PrintService {
public:
    virtual void printMessage(const std::string &msg) {
        QCOMPARE(msg.c_str(), "Hello, world!");
    }
};

class SessionServiceTestImpl : public SessionService {
    int _pings;
public:
    SessionServiceTestImpl() : _pings(0) {
        qDebug() << "SessionServiceTestImpl constructor called";
    }

    virtual void ping() {
        ++_pings;
    }

    virtual void checkPingCount(int pings) {
        QCOMPARE(_pings, pings);
    }
};

namespace funpp {

class TestConfig : public service_cfg_base {};
configure_service(TestConfig, PrintService, PrintServiceTestImpl, singleton);
configure_service(TestConfig, SessionService, SessionServiceTestImpl, session);

funpp::service_locator<funpp::TestConfig> theCfg;

template<> funpp::service_locator<funpp::TestConfig>& getServiceLocator<funpp::TestConfig>() {
    return theCfg;
}

}

DiTest::DiTest(QObject *parent) :
    QObject(parent) {
}

void DiTest::serviceLocator() {
    funpp::service_locator<funpp::TestConfig> cfg;

    auto svc = cfg.get<PrintService>();

    svc->printMessage("Hello, world!");
}

void DiTest::serviceRef() {
    funpp::service_ref<PrintService, funpp::TestConfig> svcRef;

    svcRef->printMessage("Hello, world!");
}

void DiTest::sessionService() {
    funpp::service_locator<funpp::TestConfig> cfg;

    auto svc = cfg.get<SessionService>("1");
    svc->ping();
    svc->ping();
    svc->checkPingCount(2);
    cfg.drop_session("1");
    svc = cfg.get<SessionService>("1");
    svc->ping();
    svc->checkPingCount(1);
}

std::string getSession() {
    return "1";
}

namespace funpp {

template<typename svc, typename cfg> struct session_service_holder : public service_holder<svc, cfg> {

    typename service_holder<svc,cfg>::svc_ref _svc;

    svc* get(service_locator<cfg>& c) {
        _svc = c.template get<svc>(getSession());
        return _svc.get();
    }
};

template<typename svc, typename cfg> using session_ref
    = service_ref_base<svc, cfg, session_service_holder<svc, cfg>>;
}

void DiTest::sessionServiceRef() {
    funpp::session_ref<SessionService, funpp::TestConfig> svcRef;
    svcRef->ping();
    svcRef->ping();
    svcRef->checkPingCount(2);
    funpp::session_ref<SessionService, funpp::TestConfig> svcRef1;
    svcRef1->ping();
    svcRef1->checkPingCount(3);
    funpp::getServiceLocator<funpp::TestConfig>().drop_session(getSession());
    svcRef1->ping();
    svcRef->checkPingCount(1);
}

QTEST_MAIN(DiTest)
#include "ditest.moc"
