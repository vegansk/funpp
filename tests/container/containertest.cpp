#include "containertest.h"
#include <QtTest/QtTest>

#include <funpp/container.hpp>
#include <vector>
#include <list>
#include <string>

template<typename T> using vector = funpp::container<T, std::vector>;
template<typename T> using list = funpp::container<T, std::list>;

ContainerTest::ContainerTest(QObject *parent) :
    QObject(parent) {
}

void ContainerTest::initialization() {
    auto v = vector<int>{1, 2, 3, 4, 5, 6};
    QCOMPARE((int)v.get_c().size(), 6);

    auto l = list<std::string>{"Hello", "world"};
    QCOMPARE((int)l.get_c().size(), 2);
}

QTEST_MAIN(ContainerTest)
#include "containertest.moc"
