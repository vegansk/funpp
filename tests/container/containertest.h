#ifndef CONTAINERTEST_H
#define CONTAINERTEST_H

#include <QObject>

class ContainerTest : public QObject {
    Q_OBJECT
public:
    explicit ContainerTest(QObject *parent = 0);

signals:

private slots:
     void initialization();
};

#endif // CONTAINERTEST_H
